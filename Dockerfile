FROM node:current-slim as build-stage

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY --chown=node:node package*.json ./

USER node

RUN npm install --force

#USER node

COPY --chown=node:node . .

#EXPOSE 3000

ARG FRONTEND_ENV=production

ENV VUE_APP_ENV=${FRONTEND_ENV}

RUN npm run build

#NGINX PART

FROM nginx:latest

COPY --from=build-stage /home/node/app/dist/ /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/conf.d/default.conf

#CMD [ "npm", "run", "serve" ]
