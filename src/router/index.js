import { createRouter, createWebHistory } from 'vue-router'
import DashboardView from '../views/DashboardView.vue'
import BookingsView from '../views/BookingsView.vue'
import LoginFormView from '../views/LoginFormView.vue'
import RegistrationFormView from '../views/RegistrationFormView.vue'
import EventsView from '../views/EventsView.vue'
const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: DashboardView
  },
  {
    path: '/register',
    name: 'register',
    component: RegistrationFormView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginFormView
  },
  {
    path: '/events',
    name: 'events',
    component: EventsView
  },
  {
    path: '/bookings',
    name: 'bookings',
    component: BookingsView
  }
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
