import axios from 'axios'
// import https from 'https'
export default axios.create({
  baseURL: `${window.location.origin}/api`,
  // baseURL: 'http://localhost/api',
  headers: {
    'Content-type': 'application/json'
  }
  // httpsAgent: new https.Agent({
  //  rejectUnauthorized: false
  // })
})
