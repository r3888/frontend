import { createStore } from 'vuex'

export default createStore({
  state: {
    isLoggedIn: false,
    isAdmin: false
  },
  getters: {
    isLoggedIn (state) {
      return state.isLoggedIn
    },

    isAdmin (state) {
      return state.isAdmin
    }
  },
  mutations: {
    login (state) {
      state.isLoggedIn = true
    },
    setAdmin (state) {
      state.isAdmin = true
    },
    logout (state) {
      state.isLoggedIn = false
      state.isAdmin = false
    }
  },
  actions: {},
  modules: {}
})
